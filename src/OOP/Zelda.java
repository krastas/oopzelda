package OOP;
/**
 * Created by Kristina on 20.01.2017.
 */
public class Zelda {
    private int elusid;
    public Zelda(int elusid) {
        this.elusid = elusid;
    }
    public void kaklusKolliga(int kollilElusid) {
        this.elusid = this.elusid - kollilElusid;
    }
    public void prindiMituEluAlles() {

        System.out.println(this.elusid);
    }
    public void prindiKasOnElus() {
        if (this.elusid > 0) {
            System.out.println("On elus");
        }
        else {
            System.out.println("Ei ole elus");
        }
    }
}
